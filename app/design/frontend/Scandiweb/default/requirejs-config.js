/**
 * @category    Scandiweb
 * @package     Scandiweb/default
 * @author      Kristers Lielups <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (https://scandiweb.com)
 */
var config = {
    map: {
        "*": {
            "related-slider": "js/assets/related-slider",
            "header-control": "js/assets/header-control",
        }
    },
    paths: {
        'owlcarousel': "vendor/owl/owl.carousel",
    },
    shim: {
        'owlcarousel': {
            deps: ['jquery']
        }
    }
};