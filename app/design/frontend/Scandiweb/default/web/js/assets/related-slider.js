/**
 * @category    Scandiweb
 * @package     Scandiweb/default
 * @author      Kristers Lielups <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (https://scandiweb.com)
 */
define([
    'jquery',
    'jquery/ui',
    'owlcarousel'
], function ($) {
    'use strict';

    $.widget('scandi.carousel', {

        /** @inheritdoc */
        _create: function () {
            this.initializeSlider();
        },

        /**
         * Initialize related product slider
         */
        initializeSlider: function () {
            $('.owl-carousel').owlCarousel({
                rtl:true,
                loop:true,
                margin:10,
                nav:true,
                center:true,
                dots:true,
                responsive:{
                    0:{
                        items:1
                    },
                    768:{
                        items:2,
                        center:false,
                    },
                    1024:{
                        items:3
                    },
                    1280:{
                        items:4
                    }
                }
            })
        }
    });

    return $.scandi.carousel;
});
