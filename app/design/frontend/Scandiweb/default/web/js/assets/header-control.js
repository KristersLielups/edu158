/**
 * @category    Scandiweb
 * @package     Scandiweb/default
 * @author      Kristers Lielups <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (https://scandiweb.com)
 */
define([
    'jquery'
], function ($) {
    'use strict';

    $.widget('scandi.header', {

        /** @inheritdoc */
        _create: function () {
            this.initializeHeader();
        },

        /**
         * Initialize related product slider
         */
        initializeHeader: function () {
            var head = $('header'),
                nav = $('.sections.nav-sections'),
                DEFAULT_HEAD_HEIGHT = head.height(),
                DEFAULT_NAV_TOP = nav.css('top');
            head.addClass('sticky');
            if ( $(window).width() > 767) {
                nav.addClass('sticky');
                nav.css('top', head.height() + 'px');
                $('body').css('padding-top', head.height() + nav.height() + 'px');
                $(window).scroll(function() {
                    if ($(this).scrollTop() >= 400){
                        head.animate({
                            height: 0
                        }, { duration: 1000, queue: false }, function () {
                            head.hide();
                        });
                        nav.animate({
                            top: 0
                        }, { duration: 1000, queue: false })
                    }
                    else{
                        head.show();
                        head.animate({
                            height: DEFAULT_HEAD_HEIGHT
                        }, { duration: 1000, queue: false });
                        nav.animate({
                            top: DEFAULT_NAV_TOP
                        }, { duration: 1000, queue: false })
                    }
                });
            }
            else {
                $('body').css('padding-top', head.height() + 'px');
                $(window).scroll(function() {
                    if ($(this).scrollTop() >= 400){
                        head.animate({
                            height: 0
                        }, { duration: 1000, queue: false }, function () {
                            head.hide();
                        });
                    }
                    else{
                        head.show();
                        head.animate({
                            height: DEFAULT_HEAD_HEIGHT
                        }, { duration: 1000, queue: false });
                    }
                });
            }
        }
    });

    return $.scandi.header;
});
